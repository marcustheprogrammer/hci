<?php
echo '
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php">Register App</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
           <li><a href="/login.php">Login</a></li>
            <li><a href="/signup.php">Sign-Up<span class="sr-only">(current)</span></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
					  <li><a href="/customer/cart.php">Cart</a></li>
          </ul>
					    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">More
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Employment</a></li>
          <li><a href="#">File a Complaint</a></li>
					 <li><a href="#">Other non-crucial option</a></li>
        </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
';
?>