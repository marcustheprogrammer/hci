<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
<div class="jumbotron text-center">Shared Inventory Page</div>
<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../index.php">Start</a></li>
        <li class="breadcrumb-item"><a href="../login.php">Login</a></li>
        <li class="breadcrumb-item active">Inventory</li>
    </ol>

    <div class="input-group input-group-lg">
        <span class="input-group-addon" id="sizing-addon1">Search</span>
        <input type="text" class="form-control" placeholder="Search by item name or by barcode" aria-describedby="sizing-addon1">
    </div>

    <br>
    <button type="button" class="btn btn-primary pull-right">Search</button>
    
    <br><br><br>
    <div class="alert alert-warning" role="alert">
        <strong>Note!</strong> This is an example of a search for the keyword "Bread". Some actions will not be available for certain users. 
    </div>
        
    <table class="table table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Item</th>
      <th>Units</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>ACME Bread</td>
      <td>1</td>
      <td></td>
      <td>
          <div class="btn-group-sm" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-success">Reorder</button>
            <button type="button" class="btn btn-danger">Out of stock</button>
          </div>
        </td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>ACME Bread Whole Wheat</td>
      <td>0</td>
      <td><span class="badge badge-danger">Out of Stock</span></td>
      <td>
          <div class="btn-group-sm" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-success">Reorder</button>
            <button type="button" class="btn btn-danger">Out of stock</button>
          </div>
        </td>
    </tr>
  </tbody>
</table>
    </div>
</body>
</html>