<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Search</title>
</head>

<body>
	<?php require_once('../php/header.php'); ?>
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item active">Search</li>
			</ol>
			<div class="input-group input-group-lg">
				<span class="input-group-addon" id="sizing-addon1">Search</span>
				<input type="text" class="form-control" placeholder="Search by item name or by barcode" aria-describedby="sizing-addon1">
			</div>

			<br>
			<button type="button" class="btn btn-primary pull-right">Search</button>

			<br>
			<br>
			<br>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Item</th>
						<th>Units</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Kit Kat</td>
						<td>4</td>
						<td></td>
						<td class="col-md-3">
							<div class="btn-group">
								<a href="#" class="btn btn-primary">View</a>
								<a href="#" class="btn btn-success">Add to Cart</a>
							</div>

						</td>
					</tr>
				</tbody>
			</table>
		</div>
</body>
</html>