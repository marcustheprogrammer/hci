<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Kit Kat</title>
</head>

<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Register App</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="../index.php">Home</a></li>
					<!--  class="active" -->
					<li><a href="sales.php">Sales</a></li>
					<li><a href="browse.php">Browse</a></li>
					<li><a href="search.php">Search</a></li>
					<li><a href="cart.php">Cart</a></li>
					<li><a href="../employee/employee.php">Employee</a></li>
					<li><a href="../manager/manager.php">Manager</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../login.php">Login</a></li>
					<li><a href="../signup.php">Sign-Up<span class="sr-only">(current)</span></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
			<li class="breadcrumb-item"><a href="sales.php">Sales</a></li>
			<li class="breadcrumb-item active">Kit Kat</li>
		</ol>

		<!-- BEGIN ROW -->
		<div class="row">

			<div class="col-sm-4">
				<div class="card">
					<div class="card-block">
						<h4 class="card-title">Kit Kat</h4>
					</div>

					<img class="card-img-top" src="../images/kitkat.jpg" alt="Card image cap" height=204px>
					<div class="some-space"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<div class="card-block">
						<h4 class="card-title">Description</h4>
					</div>

					<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit sapien mollis elementum dictum. Nullam egestas lacinia euismod. Praesent a euismod felis. Ae</p>
					<div class="some-space"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<div class="card-block">
						<h4 class="card-title">Aisle 4 West Shelf</h4>
					</div>
					<img class="card-img-top" src="../images/map.jpg" alt="Card image cap">

					<div class="some-space"></div>
				</div>
			</div>

			<!-- END ROW-->
		</div>

		<!-- BEGIN ROW -->
		<div class="row">

			<div class="col-sm-4">
				<div class="card">
					<p class="card-text">
						<strong>Current Price</strong> $1.45
						<br />
						<strong>Normal Price</strong> $3.45
						<br />
						<strong>Stock</strong> 4
						<br />
					</p>
					<a href="cart.php" class="btn btn-primary btn-sm">Add to cart for home delivery</a>
					<a href="cart.php" class="btn btn-primary btn-sm">Add to cart for instore pickup</a>
					<div class="some-space"></div>
				</div>
			</div>



			<!-- END ROW-->
		</div>

	</div>
</body>
</html>