<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Candy</title>
</head>

<body>
	<?php require_once('../php/header.php'); ?>
		<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item"><a href="browse.php">Browse</a></li>
				<li class="breadcrumb-item active">Candy</li>
			</ol>

			<!-- BEGIN ROW -->
			<div class="row">

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Kit Kat</h4>
						</div>

						<div class="icons" style="background-image: url('../images/kitkat.jpg')"></div>
						<p class="card-text"></p>
						<a href="itemc.php" class="btn btn-primary btn-block">$1.45 - More Details</a>
						<br />
						<div class="btn-group btn-group-justified">
							<a href="#" class="btn btn-default">Home delivery</a>
							<a href="#" class="btn btn-default">In store pickup</a>
						</div>
						<div class="some-space"></div>
					</div>
				</div>

				<!-- END ROW-->
			</div>

			<br />
			<div class="text-center">
				<nav aria-label="...">
					<ul class="pagination ">
						<li class="page-item disabled">
							<a class="page-link" href="#" tabindex="-1">Previous</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item active">
							<a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
						</li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#">Next</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- END ROW-->
		</div>

		</div>
</body>
</html>