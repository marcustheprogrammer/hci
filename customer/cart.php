<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Cart</title>
</head>

<body>
	<?php require_once('../php/header.php'); ?>

		<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item active">Cart</li>
			</ol>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Item</th>
						<th>Units</th>
						<th>Price</th>
						<th>Actions</th>
						<th>Delivery or Pickup</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>ACME Bread</td>
						<td>1</td>
						<td>$123.00</td>
						<td>
							<div class="btn-group-sm" role="group" aria-label="Basic example">
								<button type="button" class="btn btn-primary">Update</button>
								<button type="button" class="btn btn-danger">Remove</button>
							</div>
						</td>
						<td>Delivery</td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>ACME Bread Whole Wheat</td>
						<td>4</td>
						<td>$23.23</td>
						<td>
							<div class="btn-group-sm" role="group" aria-label="Basic example">
								<button type="button" class="btn btn-primary">Update</button>
								<button type="button" class="btn btn-danger">Remove</button>
							</div>
						</td>
						<td>Pickup</td>
					</tr>
				</tbody>
			</table>


			<div class="card">
				<p class="card-text">
					<strong>Subtotal</strong> $1444.45
					<br />
					<strong>Delivery Estimate</strong> 3/3/2019
					<br />
					<strong>Pickup Time</strong> 6:00 PM
					<br />
				</p>
				<a href="../index.php" class="btn btn-primary btn-sm">Checkout</a>
				<a href="../index.php" class="btn btn btn-danger btn-sm">Discard</a>
				<div class="some-space"></div>
			</div>





		</div>
</body>
</html>