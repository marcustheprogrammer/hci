<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Send Message</title>
</head>
<script>
	function resolve() {
		alert("Succesfully resolved!");
	}
</script>

<body>
	<?php require_once('../php/header.php'); ?>
		<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item"><a href="employee.php">Employee</a></li>
				<li class="breadcrumb-item"><a href="messagecenter.php">Message Center</a></li>
				<li class="breadcrumb-item active">Send Message</li>
			</ol>
			<form>

				Send Message To:
				<br>
				<br>
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Select Employee
						<span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Doe, Alice</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Smith, Bob</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Bird, Junior</a></li>
					</ul>
				</div>
				<br>
				<br>

				<div class="form-group">
					<label for="comment">Comment:</label>
					<textarea class="form-control" rows="5" id="comment"></textarea>
				</div>

			</form>
			<br />
			<a href="sendmessage.php" class="btn btn-success pull-right">Send New Message</a>

		</div>
</body>
</html>