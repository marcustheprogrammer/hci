<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <title>Scanner</title>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Register App</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="../index.php">Home</a></li> <!--   -->
            <li><a href="../customer/sales.php">Sales</a></li>
            <li><a href="../customer/browse.php">Browse</a></li>
            <li><a href="../customer/search.php">Search</a></li>
            <li><a href="../customer/cart.php">Cart</a></li>
            <li><a class="active" href="employee.php">Employee</a></li>
            <li><a href="../manager/manager.php">Manager</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../login.php">Login</a></li>
            <li><a href="../signup.php">Sign-Up<span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="employee.php">Employee</a></li>
        <li class="breadcrumb-item active">Scanner</li>
    </ol>

<div class="input-group input-group-lg">
  <span class="input-group-addon" id="sizing-addon1">Barcode</span>
  <input type="text" class="form-control" placeholder="Scan bardcode or enter manually or enter name to search" aria-describedby="sizing-addon1">
</div>

<br>
<button type="button" class="btn btn-primary pull-right">Add Item</button>

<br><br> 
    
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Item</th>
      <th>Price</th>
      <th>Qty</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>ACME Bread</td>
      <td>1.12</td>
      <td>1</td>
      <td><button type="button" class="btn btn-danger pull-right">Remove</button></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>ACME Whole Milk</td>
      <td>3.00</td>
      <td>3</td>
      <td><button type="button" class="btn btn-danger pull-right">Remove</button></td>
    </tr>
  </tbody>
</table>

<ul class="list-group">
  <li class="list-group-item active">Details</li>
  <li class="list-group-item"><strong>Tax:</strong> Value1</li>
  <li class="list-group-item"><strong>Discounts:</strong> Value2</li>
  <li class="list-group-item"><strong>Total:</strong> Value3</li> 
</ul>
    <button type="button" class="btn btn-success">Checkout</button>
    </div>
</body>
</html>