<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Message Center</title>
</head>
<script>
	function resolve() {
		alert("Succesfully resolved!");
	}
</script>

<body>
	<?php require_once('../php/header.php'); ?>
		<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item"><a href="employee.php">Employee</a></li>
				<li class="breadcrumb-item active">Message Center</li>
			</ol>


			<a href="sendmessage.php" class="btn btn-success">Compose New Message</a>
			<br>
			<br>

			<h4>Assigned Task</h4>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>From</th>
						<th>Message</th>
						<th>Respond</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Manager Bob</td>
						<td>Cleanup aisle 4</td>
						<td class="col-md-3">
							<a href="#" class="btn btn-danger" onclick="resolve();">Mark Task Resolved</a>
						</td>
		</div>

		</td>
		</tr>
		</tbody>
		</table>

		<h4>General Messages</h4>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>From</th>
					<th>Message</th>
					<th>Respond</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Junior Bird</td>
					<td>Those kitkats are on sale, we should buy them all out before the customers can</td>
					<td class="col-md-3">
						<div class="btn-group">
							<a href="#" class="btn btn-primary" onclick="resolve();">Resolve</a>
							<a href="sendmessage.php" class="btn btn-success">Reply</a>
						</div>

					</td>
				</tr>
			</tbody>
		</table>


		</div>
</body>
</html>