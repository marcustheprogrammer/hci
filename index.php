<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/main.css" type="text/css">
	<title>Home</title>
</head>

<body>
	<?php require_once('php/header.php'); ?>
		<!---<div class="jumbotron text-center">
        Customer Page
    </div> -->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item active">Home</li>
			</ol>

			<!-- BEGIN ROW -->
			<div class="row">

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Current Sales</h4>
						</div>

						<div class="icons" style="	background-image: url(images/dollar.jpeg);"></div>
						<!-- <img class="card-img-top" src="images/dollar.jpeg" alt="Card image cap"> -->
						<p class="card-text"></p>
						<a href="customer/sales.php" class="btn btn-primary btn-block">View Sales</a>
						<div class="some-space"></div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Browse</h4>
						</div>

						<!-- <img class="card-img-top" src="images/cap.jpg" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/cap.jpg);"></div>
						<p class="card-text"></p>
						<a href="customer/browse.php" class="btn btn-primary btn-block">Browse</a>
						<div class="some-space"></div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Search</h4>
						</div>

						<!-- <img class="card-img-top" src="images/search.png" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/search.png);"></div>
						<p class="card-text"></p>
						<a href="customer/search.php" class="btn btn-primary btn-block">Search</a>
						<div class="some-space"></div>
					</div>
				</div>

				<!-- END ROW-->
			</div>
			<div class="alert alert-warning">
				<strong>Note!</strong> The cards below are only shown when signed in
			</div>

			<!-- BEGIN ROW -->
			<div class="row">

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">View Cart</h4>
						</div>

						<!--<img class="card-img-top" src="images/cart.png" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/cart.png);"></div>
						<p class="card-text"></p>
						<a href="customer/cart.php" class="btn btn-primary btn-block">View Cart</a>
						<div class="some-space"></div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Sign Out</h4>
						</div>

						<!-- <img class="card-img-top" src="images/signout.jpg" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/signout.jpg);"></div>
						<p class="card-text"></p>
						<a href="#" class="btn btn-primary btn-block">Sign Out</a>
						<div class="some-space"></div>
					</div>
				</div>





				<!-- END ROW-->
			</div>
			<div class="alert alert-warning">
				<strong>Note!</strong>he cards below are only shown when signed in on an employee or store manager account. Manager is-a employee so they will see both cards.
			</div>

			<!-- BEGIN ROW -->
			<div class="row">
				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Employee Area</h4>
						</div>

						<!-- <img class="card-img-top" src="images/cap.jpg" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/cap.jpg);"></div>
						<p class="card-text"></p>
						<a href="employee/employee.php" class="btn btn-primary btn-block">Employee Area</a>
						<div class="some-space"></div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Store Manager Area</h4>
						</div>

						<!-- <img class="card-img-top" src="images/cap.jpg" alt="Card image cap"> -->
						<div class="icons" style="	background-image: url(images/cap.jpg);"></div>
						<p class="card-text"></p>
						<a href="manager/manager.php" class="btn btn-primary btn-block">Store Manager Area</a>
						<div class="some-space"></div>
					</div>
				</div>


				<!-- END ROW-->
			</div>
		</div>
</body>
</html>
