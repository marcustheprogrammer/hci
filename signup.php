<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Signup</title>

</head>

<body>
	<?php require_once('php/header.php'); ?>

		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Home</a></li>
				<li class="breadcrumb-item active">Sign-Up</li>
			</ol>

			<form>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 form-control-label">Username</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputEmail3" placeholder="Username">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 form-control-label">Store ID</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputEmail3" placeholder="Store ID">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 form-control-label">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 form-control-label">Verify Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 form-control-label"></label>
					<div class="col-sm-10">
						<button type="button" class="btn btn-primary ">Sign-Up</button>
					</div>
				</div>
			</form>
		</div>
</body>

</html>