<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

</head>
<script>
	function reorder() {
		prompt("Please enter number to order", "0");
		alert("Succesfully ordered");
	}

	function updateorder() {
		prompt("Please enter amount to add or subtract", "0");
		alert("Succesfully updated");
	}
</script>

<div class="container">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
		<li class="breadcrumb-item"><a href="manager.php">Manager</a></li>
		<li class="breadcrumb-item active">Inventory Management</li>
	</ol>

	<div class="form-group">
		<label for="usr">Barcode:</label>
		<input type="text" class="form-control" id="usr">
	</div>
	<div class="form-group">
		<label for="usr">Name:</label>
		<input type="text" class="form-control" id="usr">
	</div>
	<div class="form-group">
		<label for="usr">Starting Stock:</label>
		<input type="text" class="form-control" id="usr">
	</div>

	<br>
	<button type="button" class="btn btn-success">Add item</button>

	<br>
	<br>
	<br>

	<h3>Inventory Warnings</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Item</th>
				<th>Units</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td>ACME Bread</td>
				<td>1</td>
				<td><span class="badge badge-danger">Low Stock</span></td>
				<td>
					<div class="btn-group-sm" role="group" aria-label="Basic example">
						<button onclick="updateorder()" type="button" class="btn btn-primary">Update</button>
						<button onclick="reorder()" type="button" class="btn btn-success">Reorder</button>
					</div>
				</td>
			</tr>
			<tr>
				<th scope="row">2</th>
				<td>ACME Bread Whole Wheat</td>
				<td>0</td>
				<td><span class="badge badge-danger">Out of Stock</span></td>
				<td>
					<div class="btn-group-sm" role="group" aria-label="Basic example">
						<button onclick="updateorder()" type="button" class="btn btn-primary">Update</button>
						<button onclick="reorder()" type="button" class="btn btn-success">Reorder</button>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>

</html>