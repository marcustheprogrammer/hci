<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Add Employee</title>
</head>
<script>
	function promote() {
		alert("Employee is now a manager");
	}

	function changeSalary() {
		prompt("Please enter new salary", "0");
		alert("Succesfully updated");
	}
</script>

<body>

	<?php require_once('../php/header.php'); ?>

		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item"><a href="manager.php">Manager</a></li>
				<li class="breadcrumb-item active">Inventory Management</li>
			</ol>

			<h2>Add New Employee</h2>
			<div class="form-group">
				<label for="usr">First Name:</label>
				<input type="text" class="form-control" id="usr">
			</div>
			<div class="form-group">
				<label for="usr">Last Name:</label>
				<input type="text" class="form-control" id="usr">
			</div>
			<div class="form-group">
				<label for="pwd">Hourly Rate:</label>
				<input type="number" class="form-control" id="pwd">
			</div>
			<div class="form-group">
				<label for="pwd">Position:</label>
				<div class="radio">
					<label>
						<input type="radio" name="optradio">Standard Employee</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="optradio">Manager</label>
				</div>
			</div>


			<br>
			<button type="button" class="btn btn-success">Add Employee</button>

			<br>
			<br>
			<br>

			<h2>Current Staff</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Employee Number</th>
						<th>Employee Name</th>
						<th>Hourly Rate</th>
						<th>Is Manager</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Junior Bird</td>
						<td>$1.40</td>
						<td>No</td>
						<td>
							<div class="btn-group-sm" role="group" aria-label="Basic example">
								<button onclick="promote()" type="button" class="btn btn-primary">Promote</button>
								<button onclick="changeSalary()" type="button" class="btn btn-success">Give Raise</button>
								<button onclick="changeSalary()" type="button" class="btn btn-danger">Terminate</button>
							</div>
						</td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>Bob Smith</td>
						<td>$104.00</td>
						<td>Yes</td>
						<td>
							<div class="btn-group-sm" role="group" aria-label="Basic example">
								<button onclick="promote()" type="button" class="btn btn-primary">Promote</button>
								<button onclick="changeSalary()" type="button" class="btn btn-success">Give Raise</button>
								<button onclick="changeSalary()" type="button" class="btn btn-danger">Terminate</button>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
</body>

</html>