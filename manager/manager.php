<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/main.css" type="text/css">

	<title>Manager</title>
</head>
<script>
	function clockOut() {
		alert("Succesfully clocked out!");
	}

	function clockIn() {
		alert("Succesfully clocked in!");
	}
</script>

<body>
	<?php require_once('../php/header.php'); ?>
		<!--<div class="jumbotron text-center">
        Customer Page
    </div>-->
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="../index.php">Home</a></li>
				<li class="breadcrumb-item active">Manager</li>
			</ol>

			<!-- BEGIN ROW -->
			<div class="row">

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Inventory Management</h4>
						</div>

						<div class="icons" style="background-image: url('../images/clipboard.jpg')"></div>
						<p class="card-text"></p>
						<a href="inventory.php" class="btn btn-primary btn-block">Inventory Management</a>
						<div class="some-space"></div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Assign Tasks</h4>
						</div>

						<div class="icons" style="background-image: url('../images/cap.jpg')"></div>
						<p class="card-text"></p>
						<a href="task.php" class="btn btn-primary btn-block">Assign Tasks</a>
						<div class="some-space"></div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="card-block">
							<h4 class="card-title">Add New Employee</h4>
						</div>

						<div class="icons" style="background-image: url('../images/cap.jpg')"></div>
						<p class="card-text"></p>
						<a href="addemp.php" class="btn btn-primary btn-block">Add New Employee</a>
						<div class="some-space"></div>
					</div>
				</div>


				<!-- END ROW-->
			</div>

		</div>
</body>
</html>